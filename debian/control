Source: libpoe-component-schedule-perl
Section: perl
Priority: optional
Maintainer: FusionDirectory Maintenance Team <packages@lists.fusiondirectory.org>
Uploaders: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>,
           Benoit Mortier <benoit.mortier@opensides.be>,
           Mike Gabriel <sunweaver@debian.org>,
Build-Depends:
 debhelper-compat (= 12),
 libmodule-build-perl,
 perl,
Build-Depends-Indep:
 libdatetime-perl (>= 0.48),
 libdatetime-set-perl (>= 0.25),
 libdatetime-timezone-perl (>= 1.13),
 libpoe-perl (>= 1.287),
 libtest-pod-perl (>= 1.22),
 libtest-pod-coverage-perl (>= 1.08),
Standards-Version: 4.2.1
Homepage: https://search.cpan.org/dist/POE-Component-Schedule/
Vcs-Git: https://salsa.debian.org/debian/libpoe-component-schedule-perl.git
Vcs-Browser: https://salsa.debian.org/debian/libpoe-component-schedule-perl/

Package: libpoe-component-schedule-perl
Architecture: all
Depends:
 ${misc:Depends},
 ${perl:Depends},
 libdatetime-perl (>= 0.48),
 libdatetime-set-perl (>= 0.25),
 libdatetime-timezone-perl (>= 1.13),
 libpoe-perl (>= 1.287),
Description: Schedule POE events using DateTime::Set iterators
 This perl module is a POE component that sends events to POE client sessions on
 a schedule defined by a DateTime::Set iterator.
 .
 This module originally started as POE::Component::Cron and got forked in order
 to extract the generic parts and isolate the Cron specific code in order to
 reduce dependencies on other Perl modules. Nowadays, POE::Component::Cron
 inherits from POE::Component::Schedule.
 .
 This package provides the Perl module Poe::Component::Schedule.
